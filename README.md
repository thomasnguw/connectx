# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

A game played on an arbitrary sized board where an arbitrary number of players take turns putting tokens in columns that collect tokens at the bottom like gravity would. First player to connect a chosen number of tokens in a line or diagonal wins.
All these arbitrary numbers can be set in the main java file - the default is a 4x4 board with a 4 streak to win and 2 players.
Some people would say this game is similar to the iconic connection-game "Connect Four", they're not wrong.

### How do I get set up? ###

* Summary of set up

Run ConnectX.java

or

From the command line opened at the folder containing the connectx folder

"javac connectx/*.java" to build

"java connectx/ConnectX" to run



* Configuration

Change the constants inside the ConnectX.java file to change the board size, number of players, and win clauses.

* Dependencies

junit, this was made with junit 4.11 - You don't need to have junit if you don't want to verify the tests pass

javac if you're using bash


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
package connectx;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ConnectBoardis an implementation of a connection-game board
 * 
 * A connection-game board can be described as a set of locations 
 * that have been marked by players and the respective marking of those
 * players in chronological orger.
 * 
 * 
 * Abstract Invariant:
 * Each Location in the board must be that of a player's number, and there can be no 
 * locations marked in the board that are out of the board's bounds
 * Furthermore, a board should not be allowed to be played on after a win.
 * A board should not have tokens placed in an order that doesn't make sense according
 * to a connection-game. (Usually this means gravity should be a feature)- but this implementation
 * does not restrict your ability to play a game where you can place tokens not according to gravity
 *  
 * @author Thomas
 *
 */
public class ConnectBoard {
	public static final int BOARD_HEIGHT = 4;
	public static final int BOARD_WIDTH = 4;
	
	// how many tokens are needed in a row for player to win
	public static final int WIN_LENGTH = 4;
	// number of players in rotation
	public static final int NUM_PLAYERS = 2;
	// marks that board is in win or draw state and cannot be modified
	private boolean winLock = false;
	
	// board contains info on 
	private final Map<Point, Integer> board;
	private final List<Integer> moves;
	
	/**
	 * @effects: constructs new empty board, no moves done
	 */
	public ConnectBoard() {
		this.board = new HashMap<Point, Integer>();
		this.moves = new ArrayList<Integer>();
	}
	
	/**
	 * check if location was played by player on board
	 * @modifies none
	 * @requires board != null 
	 * @returns true if player placed token at that position
	 */
	public boolean containsKey(Point location) {
		return this.board.containsKey(location);
	}
	
	/**
	 * Add Point to boardif possible
	 * Allows positions to be overwritten by adding new tokens to same point
	 * @param Point location, location for player to place their token
	 * @param Integer player, player number palcing the token
	 * @modifies this, boards
	 * @requires board != null && graph doesn't already contain node name
	 * @throws NullPointerException if player or location if null
	 */
	public void addPoint(Point location, Integer player) {
		if (location == null || player == null) 
			throw new NullPointerException("Null argument in adding token");
		this.board.put(location, player);
		this.moves.add(new Integer(location.x + 1));
	}
	
	/**
	 * Get token at location on board
	 * @param Point location, location to check for token
	 * @modifies none
	 * @requires board != null && location != null
	 * @throws NullPointerException if player or location if null
	 */
	public Integer getLocationRes(Point location) {
		if (location == null) 
			throw new NullPointerException("Null argument in getting token");
		Integer token = this.board.get(location);
//		System.out.println("TOKEN IS: " + location);
		if (token == null) {
			return new Integer(0);
		} else {
			return new Integer(token.intValue());
		}
	}
	
	public void printBoard() {
		// iterate through board map, print if player moved there their number else 0		
		for (int i = BOARD_HEIGHT - 1; i >=0; i--) {					
			System.out.print("\n|");
			for (int j = 0; j < ConnectBoard.BOARD_WIDTH; j++) {
				Integer oldMove = getLocationRes(new Point(j,i));
				if (oldMove == null) {
					System.out.print(" 0");
				} else {
					System.out.print(" " + oldMove.toString());
				}
			}
		}
		System.out.print("\n+");
		for (int i = 0; i < BOARD_WIDTH * 2; i++) {
			System.out.print("-");
		}
		System.out.println();
	}
	

	/**
	 * Get respective moves played on board
	 * @modifies none
	 * @requires board != null 
	 * @returns list of moves played on board
	 */
	public List<Integer> getMoves() {
		// functionally return
		return new ArrayList<Integer>(moves);
	}
	
	/**
	 * Get number of moves played on board
	 * @modifies none
	 * @requires board != null 
	 * @returns number of moves played on board
	 */
	public int getMovesSize() {
		// so you dont waste time copying moves list..
		return moves.size();
	}
	
	/**
	 * check lock status of board
	 * @modifies none
	 * @requires board != null 
	 * @returns lock status of board
	 */
	public boolean getWinLock() {
		return this.winLock;
	}
	
	/**
	 * set lock on win status of board
	 * @modifies board
	 * @requires board != null 
	 * @returns none
	 */
	public void setLock() {
		this.winLock = true;
	}
}

package connectx;

import java.util.Scanner;
import connectx.ConnectBoard;

/**
 * Game of connect four, start on command line with java or your favorite environment
 * @author tom
 */
public class ConnectX {
	private static ConnectBoard board;
	
	// user commands must fit exactly the commands listed, *all caps is crucial*
	// will tell if user inputs invalid input..
	// doesn't have help window because spec doesn't say and you wouldn't be able to revert it in later
	// versions
	public static void main(String[] args) {
	// will track board, placed tokens	
		board = new ConnectBoard();
		System.out.println("Hello and welcome to the game, commands are: EXIT, GET, BOARD, and PUT #");
		Scanner scan = new Scanner(System.in);
		// split on 1 or more spaces
		String[] userCommand = scan.nextLine().split("\\s+");
		int curUserInd = 0;
		while (!singularCommand(userCommand, "EXIT")) {
			// I want to strtok to arr
			if (userCommand[0].equals("PUT")) {
				if (ConnectLogic.HandlePut(board, userCommand, curUserInd + 1)) {
					curUserInd = (curUserInd + 1) % ConnectBoard.NUM_PLAYERS;
				}				
			} else if (singularCommand(userCommand, "GET")) {
				for (Integer i: board.getMoves())
					System.out.println(i);
			} else if (singularCommand(userCommand, "BOARD")) {
				// iterate through board map, print if player moved there their number else 0
				board.printBoard();
			} else {
				// System.out.println("INVALID COMMAND - PLEASE TRY AGAIN");
				// technically undefined behavior
				System.out.println("ERROR");			
			}			
			userCommand = scan.nextLine().split("\\s+");
		}
		scan.close();
	}
	
	/**
	 * checks if user command is a singular command
	 * @param arr array of user command tokened by whitespace
	 * @param cmd command for first element of array to be verified against
	 * @return true if command is a single command and user command matches command
	 */
	private static boolean singularCommand(String[] arr, String cmd) {
		return arr.length == 1 && arr[0].equals(cmd);
	}
	
}

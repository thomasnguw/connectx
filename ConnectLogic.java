package connectx;

import java.awt.Point;
import connectx.ConnectBoard;

/**
 * Methods for modification of board according to user command strings without
 * breaking the underlying board
 * @author tom
 *
 */
public class ConnectLogic {
	

	/**
	 * Tries to put player token into board for tracking
	 * @param Board playing on
	 * @param  userCommand commands user input
	 * @param playerNum actual player number placing token 
	 * @modifies this, board
	 * @requires board != null
	 * @return true if player put their token and the next player can play, else false
	 */
	public static boolean HandlePut(ConnectBoard board, String[] userCommand, int playerNum) {
		// bad user input
		if (board.getWinLock() || userCommand.length != 2) {
			System.out.println("ERROR");
			return false;
		}
		// parse number and place pos
		try {
			int columnToInt = Integer.parseInt(userCommand[1]) - 1;
			// first check if possible to put in that column.. then do it and add to moves list
			// else dont add to move list and call on error
			if (columnToInt >= ConnectBoard.BOARD_WIDTH || columnToInt < 0) {
				System.out.println("ERROR");
				return false;
			}
				
			int possiblePlaceHeight = 0;
			while (possiblePlaceHeight < ConnectBoard.BOARD_HEIGHT) {
				Point attemptedPos = new Point(columnToInt, possiblePlaceHeight);
				if (!board.containsKey(attemptedPos)) {
					board.addPoint(attemptedPos, new Integer(playerNum));
//					moves.add(new Integer(columnToInt + 1));
					// CHECK WIN OR DRAW and lock accordingly.. players arent allowed to put after
					if (board.getMovesSize() == ConnectBoard.BOARD_HEIGHT * ConnectBoard.BOARD_WIDTH) {
						board.setLock();
						System.out.println("DRAW");
						return false;
					}
					// here we check win
					if (isBoardWin(board, playerNum, attemptedPos)) {
						board.setLock();
						System.out.println("WIN");
						return false;
					}
					
					// last case, no win, just a plain move
					System.out.println("OK");					
					return true;
				}
				// else continue
				possiblePlaceHeight++;
			}
			// case that column is full..
			System.out.println("ERROR");
			return false;
			
		} catch (NumberFormatException e){
			// also bad user input, not int
			System.out.println("ERROR");
			return false;
		}
	}

	
	
	/**
	 * evaluates if board is in win position for player starting at position
	 * @param Board playing on
	 * @param playerNum actual player number placing token 
	 * @param checkPos point to verify win location from
	 * @modifies none
	 * @requires board != null
	 * @return true if board is in win position for that player starting at that position
	 */
	// only need to check 1 player cause that player makes move
	public static boolean isBoardWin(ConnectBoard board, int playerNum, Point checkPos) {
		// I will say there exists a way to do this in 1 loop but it's not that readable 
		// Also I got midterms to do
		
		// span left right, updown, or diag, or diag for winlength
		int curStreak = 1;
		int startX = checkPos.x;
		int startY = checkPos.y;
		
		// might be a lot of code, but it runs in short time
		
		boolean keepRunning = true;
		// run right
		int counter = 1;
		while (counter + startX < ConnectBoard.BOARD_WIDTH && keepRunning) {
			Point checkpt = new Point(counter + startX, startY);
			if (board.containsKey(checkpt) && board.getLocationRes(checkpt).equals(playerNum)) {
				curStreak++;
				counter++;
			} else {
				keepRunning = false;
			}
		}
		
		// run left
		counter = 1;
		keepRunning = true;
		while (startX - counter >= 0 && keepRunning) {
			Point checkpt = new Point(counter - startX, startY);
			if (board.containsKey(checkpt) && board.getLocationRes(checkpt).equals(playerNum)) {
				curStreak++;
				counter++;
			} else {
				keepRunning = false;
			}
		}
		if (curStreak >= ConnectBoard.WIN_LENGTH) {
			return true;
		}
		
		// restart streak and go new dir
		counter = 1;
		keepRunning = true;
		curStreak = 1;
		// run up
		while (counter + startY < ConnectBoard.BOARD_HEIGHT && keepRunning) {
			Point checkpt = new Point(startX, counter + startY);
			if (board.containsKey(checkpt) && board.getLocationRes(checkpt).equals(playerNum)) {
				curStreak++;
				counter++;
			} else {
				keepRunning = false;
			}
		}
		
		// run down
		counter = 1;
		keepRunning = true;
		while (counter - startX >= 0 && keepRunning) {
			Point checkpt = new Point(startX, startY - counter);
			if (board.containsKey(checkpt) && board.getLocationRes(checkpt).equals(playerNum)) {
				curStreak++;
				counter++;
			} else {
				keepRunning = false;
			}
		}
		if (curStreak >= ConnectBoard.WIN_LENGTH) {
			return true;
		}

		// new streak and dir
		counter = 1;
		keepRunning = true;
		curStreak = 1;
		// run up right 
		while (counter + startY < ConnectBoard.BOARD_HEIGHT && counter + startX < ConnectBoard.BOARD_WIDTH && keepRunning) {
			Point checkpt = new Point(counter + startX, counter + startY);
			if (board.containsKey(checkpt) && board.getLocationRes(checkpt).equals(playerNum)) {
				curStreak++;
				counter++;
			} else {
				keepRunning = false;
			}
		}
		// run down left
		counter = 1;
		keepRunning = true;
		while (startY - counter >=  0 && startX - counter >= 0 && keepRunning) {
			Point checkpt = new Point(startX - counter, startY - counter);
			if (board.containsKey(checkpt) && board.getLocationRes(checkpt).equals(playerNum)) {
				curStreak++;
				counter++;
			} else {
				keepRunning = false;
			}
		}
		if (curStreak >= ConnectBoard.WIN_LENGTH) {
			return true;
		}

		
		// new streak and dir
		counter = 1;
		keepRunning = true;
		curStreak = 1;
		// run left and up
		while (counter + startY < ConnectBoard.BOARD_HEIGHT && startX - counter >= 0 && keepRunning) {
			Point checkpt = new Point(startX - counter, counter + startY);
			if (board.containsKey(checkpt) && board.getLocationRes(checkpt).equals(playerNum)) {
				curStreak++;
				counter++;
			} else {
				keepRunning = false;
			}
		}
		// run right down
		counter = 1;
		keepRunning = true;
		while (startY - counter >=  0 && startX + counter < ConnectBoard.BOARD_WIDTH && keepRunning) {
			Point checkpt = new Point(startX + counter, startY - counter);
			if (board.containsKey(checkpt) && board.getLocationRes(checkpt).equals(playerNum)) {
				curStreak++;
				counter++;
			} else {
				keepRunning = false;
			}
		}
		if (curStreak >= ConnectBoard.WIN_LENGTH) {
			return true;
		}
			
		
		return false;
		
	}

	
	
}

package connectx.tests;

import connectx.*;

import org.junit.Test;
import static org.junit.Assert.*;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;;
/**
 * This class contains a set of test cases that can be used to test the
 * implementation of the Connect* classes. It ensures that connect* classes can
 * fail and quickly with unexpected input
 */

public final class TestGraphStructure {
//	junit
	private static ConnectBoard makeBoard() {
		return new ConnectBoard();
	}
	private static Point makePoint(int x, int y) {
		return new Point(x,y);
	}
	private static Integer makePlayer(int player_number) {
		return new Integer(player_number);
	}
	
	/**
	 * Verify that a point does not exist in graph before adding it
	 * and that the point shows up after adding it
	 */
	@Test
	public void testAddAndContains() {
		ConnectBoard testBoard = makeBoard();
		Point loc = makePoint(1, 1);
		Integer player = makePlayer(5);
		assertFalse(testBoard.containsKey(loc));
		testBoard.addPoint(loc, player);
		assertTrue(testBoard.containsKey(loc));
		assertEquals(player, testBoard.getLocationRes(loc));
		
		Point nonExistantLoc = makePoint(2, 2);
		assertFalse(testBoard.containsKey(nonExistantLoc));		
	}
	
	/**
	 * Verify that point placed by player is player on board
	 */
	@Test
	public void testAddAndGet() {
		ConnectBoard testBoard = makeBoard();
		Point loc = makePoint(1, 1);
		Integer player = makePlayer(5);
		Integer falsePlayer = makePlayer(60);
		testBoard.addPoint(loc, player);
		assertEquals(player, testBoard.getLocationRes(loc));
		assertNotEquals(falsePlayer, testBoard.getLocationRes(loc));
	}
	
	
	// adding multiple is bad.. should do that on the logic test

	/**
	 * Test that the board will handle overwriting a location
	 * --note that is board logic, not the game logic
	 * --game doesnt allow this
	 */
	@Test
	public void testAddExtra() {
		ConnectBoard testBoard = makeBoard();
		Point loc = makePoint(1, 1);
		Integer initialPlayer = makePlayer(5);
		Integer secondPlayer = makePlayer(60);
		testBoard.addPoint(loc, initialPlayer);
		assertEquals(initialPlayer, testBoard.getLocationRes(loc));

		testBoard.addPoint(loc, secondPlayer);
		assertEquals(secondPlayer, testBoard.getLocationRes(loc));
		assertNotEquals(initialPlayer, testBoard.getLocationRes(loc));
	}

	
	/**
	 * Verify that moveslist corresponds with user expectation of moves list
	 * after placing moves in order
	 */
	@Test
	public void testAddMoves() {
		ConnectBoard testBoard = makeBoard();
		Integer player7 = makePlayer(7);
		Integer player8 = makePlayer(8);
		testBoard.addPoint(makePoint(1, 1), player7);
		
		List<Integer> movesList = testBoard.getMoves();
		List<Integer> actualList = new ArrayList<Integer>();
		actualList.add(2);  // actuallist is offset because players count from 1
		assertEquals(movesList, actualList);
		
		testBoard.addPoint(makePoint(1, 2), player8);
		actualList.add(2);
		movesList = testBoard.getMoves();
		assertEquals(movesList, actualList);
		
		
		testBoard.addPoint(makePoint(1, 3), player7);
		actualList.add(2);
		movesList = testBoard.getMoves();
		assertEquals(movesList, actualList);
		
		
		testBoard.addPoint(makePoint(2, 1), player8);
		actualList.add(3);
		movesList = testBoard.getMoves();
		assertEquals(movesList, actualList);
		
	}
	
	
	/**
	 * Verify that you can set board lock at any point
	 */
	@Test
	public void testBoardLocking() {
		ConnectBoard testBoard = makeBoard();
		assertFalse(testBoard.getWinLock());
		testBoard.setLock();
		assertTrue(testBoard.getWinLock());
	}
}

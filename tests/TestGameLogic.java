
package connectx.tests;

import org.junit.Test;

import connectx.*;

import static org.junit.Assert.*;

import java.awt.Point;


public final class TestGameLogic {
	private static ConnectBoard makeBoard() {
		return new ConnectBoard();
	}
	private static Point makePoint(int x, int y) {
		return new Point(x,y);
	}
	private static String[] splitstr(String s) {
		return(s.split("\\s+"));
	}
	private static Integer makePlayer(int player_number) {
		return new Integer(player_number);
	}

	/**
	 * Test that default settings of 2 players 4x4 board handles put command
	 */
	@Test
	public void testPuttingPoints() {
		ConnectBoard testBoard = makeBoard();
		assertFalse(testBoard.containsKey(makePoint(0, 0)));
		ConnectLogic.HandlePut(testBoard, splitstr("PUT 1"), 1);
		assertTrue(testBoard.containsKey(makePoint(0, 0)));
		assertEquals(testBoard.getLocationRes(makePoint(0, 0)), makePlayer(1));

		assertFalse(testBoard.containsKey(makePoint(0, 1)));
		ConnectLogic.HandlePut(testBoard, splitstr("PUT 1"), 2);
		assertTrue(testBoard.containsKey(makePoint(0, 1)));
		assertEquals(testBoard.getLocationRes(makePoint(0, 1)), makePlayer(2));
	}
	
	
	/**
	 * Test if board correctly shows win state by user putting commands
	 */
	@Test
	public void testDefaultBoardWin() {
		ConnectBoard testBoard = makeBoard();
		ConnectLogic.HandlePut(testBoard, splitstr("PUT 1"), 1);
		assertFalse(ConnectLogic.isBoardWin(testBoard, 1, makePoint(0, 0)));
		
		ConnectLogic.HandlePut(testBoard, splitstr("PUT 1"), 1);
		assertFalse(ConnectLogic.isBoardWin(testBoard, 1, makePoint(0, 1)));
		
		ConnectLogic.HandlePut(testBoard, splitstr("PUT 1"), 1);
		assertFalse(ConnectLogic.isBoardWin(testBoard, 1, makePoint(0, 2)));
		
		ConnectLogic.HandlePut(testBoard, splitstr("PUT 1"), 1);
		assertFalse(ConnectLogic.isBoardWin(testBoard, 2, makePoint(0, 3)));
		assertTrue(ConnectLogic.isBoardWin(testBoard, 1, makePoint(0, 3)));
	}
}
